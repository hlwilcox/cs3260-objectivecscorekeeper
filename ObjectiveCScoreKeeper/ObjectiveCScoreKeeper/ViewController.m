//
//  ViewController.m
//  ObjectiveCScoreKeeper
//
//  Created by Heather Wilcox on 2/11/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtfldTeam1;
@property (weak, nonatomic) IBOutlet UITextField *txtfldTeam2;
@property (weak, nonatomic) IBOutlet UILabel *lblTeam1;
@property (weak, nonatomic) IBOutlet UILabel *lblTeam2;
@property (strong, nonatomic) IBOutlet UIStepper *stprTeam1;
@property (strong, nonatomic) IBOutlet UIStepper *stprTeam2;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)team1StepperChanged:(UIStepper *)sender {
        self.lblTeam1.text = [NSString stringWithFormat:@"%.f", sender.value];
}
- (IBAction)team2StepperChanged:(UIStepper *)sender {
        self.lblTeam2.text = [NSString stringWithFormat:@"%.f", sender.value];
}
- (IBAction)resetBtn:(UIButton *)sender {
    self.lblTeam1.text = @"0";
    self.lblTeam2.text = @"0";
    self.stprTeam1.value = 0;
    self.stprTeam2.value = 0;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
