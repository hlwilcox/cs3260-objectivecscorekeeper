//
//  AppDelegate.h
//  ObjectiveCScoreKeeper
//
//  Created by Heather Wilcox on 2/11/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

